PW_SHA256 := 6183b695423a69fdd15f56a31e013e703736f37806499c158566c51c4aef654a
NEEDED_CFLAGS := -std=c99 -D_POSIX_C_SOURCE=200112L -D__EXTENSIONS__
ifeq ($(shell sha256sum pw.c),$(PW_SHA256)  pw.c)
PW_VERSION := 2
else
PW_VERSION := 2-$(shell git rev-parse --short HEAD)
PW_VERSION := $(PW_VERSION)$(shell git diff --quiet || printf -- "-dirty")
endif
CFLAGS ?= -g -O2 -W -Wall
override CFLAGS += $(NEEDED_CFLAGS) -DCONFIG_PW_VER=\"$(PW_VERSION)\"
DESTDIR ?= /usr/local
.PHONY: all fixver
all: pw fixver
pw:
clean:; rm -f pw
install:; install pw $(DESTDIR)/bin/pw; \
          install pw.1 $(DESTDIR)/share/man/man1 \
          install pw-relnotes.5 $(DESTDIR)/share/man/man5
define FIXVER
sed -e '/^\.TH/s/Version [^"]\+/Version $(PW_VERSION)/' $1 > pw.tmp ;
cmp -s $1 pw.tmp && rm pw.tmp || mv pw.tmp $1
endef
fixver:; @$(call FIXVER,pw.1); $(call FIXVER, pw-relnotes.5)
