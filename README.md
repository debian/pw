## What is `pw`?

`pw` stands for Pipe Watch. Pipe-Watch is a new monitoring utility for
Unix-like systems, developed starting in April 2022.

## Demo Video

<div style="padding:55% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/705257869?h=7e8d85a3c2&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="pipewatch-gnome-strace.mp4"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## More Info

`pw` can monitor anything that produces textual output. `tail -f /var/logfile`,
`tcpdump`, `strace`, ...

`pw` does not show you everything. Of course, it reads all the data, but it
does that in the background. It continuously pumps lines of input through a
small FIFO buffer. This buffer is sampled, and the sample is displayed.  When
that sampling occurs is controlled in various interactive ways. What goes into
the FIFO can be filtered and the filters can be edited interactively.

With `pw` you can:

* Interactively apply and remove filters on-the-fly, without interrupting
  the source.

* Make recurring patterns in the stream appear to "freeze" on the screen, using
  triggers.

* Prevent the overwhelming amount of output from a program to flooding the
  terminal, while consuming all of that output so that the program isn't
  blocked. `pw` can pause its display updates entirely.

* Juggle multiple shell background jobs that produce output, yet execute
  indefinitely without blocking. When `pw` runs as as part of a shell
  background job, it continues to consume input, process filters and take
  snapshots, without displaying anything.  When put into the foreground again,
  display resumes.

For instance the command `tcpdump -i <ethernet-device> -l | pw` turns `tcpdump`
into an interactive network monitoring tool in which you can use the dynamic
filtering in `pw` to select different kinds of packets, and use the trigger
feature to capture certain patterns of interaction.

`pw` is like an oscilloscope for text streams. Digital oscilloscopes
sample the signal and pass it through a fifo, which is sampled to the
oscilloscope screen, and can trigger the sampling on certain conditions in the
signal to make waveforms appear to stand still. `pw` does something like that
for text streams.

## Dependencies and Resource Use

* **`pw` has very low dependencies**: it requires libraries other than the
  POSIX standard functions; all terminal control is via ANSI sequences. It does
  like to have a `struct winsize` and an `ioctl` to fill it in.  There is a
  seven line GNU Makefile to build it.  If that doesn't work for you, try it
  some other way. It tested it on on Solaris 10, and older Mac OS, Cygwin and a
  few GNU/Linuxes. It built on OpenBSD, but didn't run properly; I didn't
  investigate into it. Patches welcome.

* **`pw` is tiny:** it is a little over 2000 lines of C in a single source
  file, compiling to an executable of around 32 kilobytes.

* **`pw` requires a very low amount of RAM:**  Given inputs with typical line
  lengths, and 24 line snapshots, `pw` it needs less than 64 kilobytes of RAM
  to store the data which is passing through it.

## Documentation

The [`pw` man page](../tree/pw.1) has all the usage details.

## Video: Vertical Panes Demo

<div style="padding:75% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/707385984?h=839efed1ad&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="pw-split-screen.mp4"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## High Level State Diagram

![PipeWatch State Diagram](https://www.plantuml.com/plantuml/png/ZLHFRzi-3BthKn0v_D3-bWOz3Xam55qNeCEm0kjssRe3Qp6nK5ioI3bPjlMxJx8Znuupf0S34lduXu_FkXAFxEkoK99XGV3wyX8sHljSgU___u2BYxTmZTdJYbrjDSnXahcon73y0zK6_fjqg8LZEewQADLpHkm7ztXVIqLMKqI_yrnSV5DgO1vNVbo5J3tJkEeo8aPPJc7cO9q7hgqrTgNK07xlqTS2ahkDVGFQI5NW5kfAeoU1sXOamPjIpPII443zahyLmB2Vhp4wb0hftrjFh61lYlv-P_SLPXHuOALWwF1eDR86OwlQ0nE6waF8RUEfK2HSVSwkNkMmk5tS7G8_qGR4OXMczDAGuV69R2pPZjEr5uT9nXiZ6R8EvfvnOOeYMg8fNU9GFJJCcoOQhpnOxpfQnVoYMHEQu2awPPeFi0_Ms8m9XKAUirUm4zqq92KkWIc2pXN0e9jstd0awRAPiYHjq5EnFS0ohP1risQt1CzcjI8EPmPlIdArb-b1H9GzCW7goAvtaDNCP7r7yNZ4ETmLUjVIabtP4gJPLHNfCHW5R3S2CfjrecsucRIms1uJVQwjJOH-QGU9wqz3dSav27crY-WG4J-8xo84ie8mG5BC47Mp8vTfoIGvwKXDOY5fx5YWi_14sqSNvOsPDslZjzD6uedqwV4uXiA5audxNsC1zBCol9sDHZheBISvqjeuQ9EJfJLnkvxCMK_M2vJeipnnCcl2dbNydfKQl60VZQMkI5knVLuwKTTLnIJIUqbw2U8kTyRUXb5WihzEzfYeo0Hepj6he7EqB-pB_qk05DpzHKKa49DoKVzRxjSlLtl4dXRxQIPssa-u99M4xDqd0-1U-17MtHhkO9WeliC3pO2p0vsca9VBjD_0A7iIu1XuGf5HYrTaTLqMVm40.png)
